# Net::Ethereum Perl5 module examples

## Overview

This repo contains a few example scripts powered by Perl5 Net::Ethereum.

`examples/getaccount.pl` - connects to ethereum node and gets account info.

`examples/deploycontract.pl` - deploys smart contract to blockchain. Contract name is hard-coded. Contract file should be located in the same folder. As an exmple [BigBro](https://gitlab.com/pheix-research/smart-contracts) smart contract is used.

`examples/contract-test.pl` - smart contract communication test: fetch and store data via smart contract methods.

## License information

Pheix is free and opensource software, so you can redistribute it and/or modify it under the terms of the GNU General Public License v3.0.

## Links and credits

[pheix/smart-contracts](https://gitlab.com/pheix-research/smart-contracts)

[Net::Ethereum 0.30.1](https://gitlab.com/pheix/net-ethereum)

[GPLv3.0](https://www.gnu.org/licenses/gpl-3.0.html)

## Contact

Please contact me via [feedback form](https://pheix.org/feedback.html) at pheix.org
