pragma solidity ^0.4.21;
contract BigBro {
    struct Statistics {
        uint id;
        string referer;
        string ip;
        string useragent;
        string resolution;
        string page;
        string country;
    }
    Statistics[] BigBroDB;
    function set_by_id(uint _id, string _ref, string _ip, string _ua, string _res, string _pg, string _cntry) public returns (uint status) {
        if (_id > 0) {
            for (uint i=0; i<BigBroDB.length; i++) {
                if (BigBroDB[i].id == _id) {
                    bytes memory _ref_t = bytes(_ref);
                    bytes memory _ip_t = bytes(_ip);
                    bytes memory _ua_t = bytes(_ua);
                    bytes memory _res_t = bytes(_res);
                    bytes memory _pg_t = bytes(_pg);
                    bytes memory _cntry_t = bytes(_cntry);
                    if ( _ref_t.length > 0 ) { BigBroDB[i].referer = _ref; }
                    if ( _ip_t.length > 0) { BigBroDB[i].ip = _ip; }
                    if ( _ua_t.length > 0 ) { BigBroDB[i].useragent = _ua; }
                    if ( _res_t.length > 0 ) { BigBroDB[i].resolution = _res; }
                    if ( _pg_t.length > 0 ) { BigBroDB[i].page = _pg; }
                    if ( _cntry_t.length > 0 ) { BigBroDB[i].country = _cntry; }
                    return 1;
                }
            }
        }
        return 0;
    }
    function insert(uint _id, string _ref, string _ip, string _ua, string _res, string _pg, string _cntry) public {
        BigBroDB.push(Statistics( _id, _ref, _ip, _ua, _res, _pg, _cntry ) );
    }
    function get_count() public constant returns (uint256 count) {
        return BigBroDB.length;
    }
    function get_by_id(uint _id) public constant returns (uint id, string referer, string ip, string useragent, string resolution, string page, string country) {
        if (_id > 0) {
            for (uint i=0; i<BigBroDB.length; i++) {
                if (BigBroDB[i].id == _id) {
                    return (
                        BigBroDB[i].id,
                        BigBroDB[i].referer,
                        BigBroDB[i].ip,
                        BigBroDB[i].useragent,
                        BigBroDB[i].resolution,
                        BigBroDB[i].page,
                        BigBroDB[i].country
                    );
                }
            }
        }
    }
    function get_by_index(uint index) public constant returns (uint id, string referer, string ip, string useragent, string resolution, string page, string country) {
        if (index >= 0 && index < BigBroDB.length) {
            if (BigBroDB[index].id > 0) {
                return (
                    BigBroDB[index].id,
                    BigBroDB[index].referer,
                    BigBroDB[index].ip,
                    BigBroDB[index].useragent,
                    BigBroDB[index].resolution,
                    BigBroDB[index].page,
                    BigBroDB[index].country
                );
            }
        }
        return (0, "", "", "", "", "", "");
    }
    function remove_by_id(uint _id) public returns (uint status) {
        uint rc = 0;
        if (_id > 0) {
            for (uint i=0; i<BigBroDB.length; i++) {
                if (BigBroDB[i].id == _id) {
                    BigBroDB[i] = BigBroDB[BigBroDB.length-1];
                    BigBroDB.length--;
                    rc = i;
                    break;
               }
           }
        }
        return rc;
    }
    function get_max_id() public constant returns (uint256 id) {
        uint rc = 0;
        for (uint i=0; i<BigBroDB.length; i++) {
            if (BigBroDB[i].id > rc) {
                rc = BigBroDB[i].id;
           }
        }
        return rc;
    }
    function init_db() public {
        uint id = get_max_id();
        insert( (id + 1), "goo.gl", "127.0.0.1", "Mozilla", "640*480", "index.html", "RU");
        insert( (id + 2), "twitter.com", "127.0.0.11", "IE", "1900*1280", "index2.html", "US");
        insert( (id + 3), "ya.ru", "127.0.0.12", "Opera", "1024*768", "index3.html", "BY");
        insert( (id + 4), "pheix.org", "127.0.0.111", "Safari", "100*200", "index4.html", "UA");
        insert( (id + 5), "foo.bar", "127.0.0.254", "Netscape", "1152*778", "index5.html", "RO");
    }
}
