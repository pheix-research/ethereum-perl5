#!/usr/bin/perl

# Deploy contract

use Net::Ethereum;
use Data::Dumper;

my $contract_name = 'BigBro';
my $password = 'ospasswd';
# my $contract_id = '0x9e832089181b2a5f05c41b4ff0173b0ece64ebe7';
my $contract_id = '0xb3abfa488058dba76206071b3600ae6e0dec3205';

my $node = Net::Ethereum->new('http://localhost:8600/');
print "Net::Ethereum v", $node->VERSION, "\n";
$node->set_debug_mode(0);
$node->set_show_progress(1);

# my $src_account = "0x0f687ab2be314d311a714adde92fd9055df18b48";
my $src_account = $node->eth_accounts()->[0];
print 'My account: '.$src_account, "\n";

my $abi = $node->_read_file('build/'.$contract_name.'.abi');
$node->set_contract_abi($abi);
$node->set_contract_id($contract_id);

my $function_params={};
my $test1 = $node->contract_method_call('get_count', $function_params);
print Dumper($test1);

# Send Transaction 1

my $rc = $node->personal_unlockAccount($src_account, $password, 600);
print 'Unlock account '.$src_account.'. Result: '.$rc, "\n";

# uint id;
# string referer;
# string ip;
# string useragent;
# string resolution;
# string page;
# string country;

$function_params={};
#$function_params->{ pBool } = 1;
#$function_params->{ pAddress } = "0xa3a514070f3768e657e2e574910d8b58708cdb82";
#$function_params->{ pVal1 } = 1111;
#$function_params->{ pStr1 } = "This is string 1";
#$function_params->{ pVal2 } = 222;
#$function_params->{ pStr2 } = "And this is String 2, very long string +++++++++++++++++=========";
#$function_params->{ pVal3 } = 333;
#$function_params->{ pVal4 } = '-999999999999999999999999999999999999999999999999999999999999999977777777';

my $used_gas = $node->contract_method_call_estimate_gas('init_db', $function_params);
my $gas_price=$node->eth_gasPrice();
my $transaction_price = $used_gas * $gas_price;
my $call_price_in_eth = $node->wei2ether($transaction_price);
print 'Estimate Transaction Gas: '.$used_gas.' ('.sprintf('0x%x', $used_gas).') wei, '.$call_price_in_eth.' ether', "\n";

my $tr = $node->sendTransaction($src_account, $node->get_contract_id(), 'init_db', $function_params, $used_gas);

print 'Waiting for transaction: ', "\n";
my $tr_status = $node->wait_for_transaction($tr, 25, $node->get_show_progress());
print Dumper($tr_status);

$function_params={};
my $test2 = $node->contract_method_call('get_count', $function_params);
print Dumper($test2);

my $function_params={};
my $test3 = $node->contract_method_call('get_max_id', $function_params);
print Dumper($test3);

$function_params={};
$function_params->{ _id } = 2;
my $test4 = $node->contract_method_call('get_by_id', $function_params);
print Dumper($test4);

