#!/usr/bin/perl

# web3_sha3 test

use Net::Ethereum;

# my $contract_name = $ARGV[0];
my $password = 'ospasswd';

my $node = Net::Ethereum->new('http://localhost:8600/');
print "Net::Ethereum v", $node->VERSION, "\n";
$node->set_debug_mode(0);
$node->set_show_progress(1);

my $hex = $node->web3_sha3("0x68656c6c6f20776f726c64");
print 'web3_sha3 hex: '.$hex, "\n";
