#!/usr/bin/perl

# Deploy contract

use Net::Ethereum;

# my $contract_name = $ARGV[0];
my $password = 'ospasswd';

my $node = Net::Ethereum->new('http://localhost:8600/');
print "Net::Ethereum v", $node->VERSION, "\n";
$node->set_debug_mode(0);
$node->set_show_progress(1);

# my $src_account = "0x0f687ab2be314d311a714adde92fd9055df18b48";
my $src_account = $node->eth_accounts()->[0];
print 'My account: '.$src_account, "\n";
