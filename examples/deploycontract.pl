#!/usr/bin/perl

# Deploy contract

use Net::Ethereum;

my $contract_name = 'BigBro';
my $password = 'ospasswd';

my $node = Net::Ethereum->new('http://localhost:8600/');
print "Net::Ethereum v", $node->VERSION, "\n";
$node->set_debug_mode(0);
$node->set_show_progress(1);

# my $src_account = "0x0f687ab2be314d311a714adde92fd9055df18b48";
my $src_account = $node->eth_accounts()->[0];
print 'My account: '.$src_account, "\n";

my $constructor_params={};
$constructor_params->{ initString } = '+ Init string for constructor +';
$constructor_params->{ initValue } = 102; # init value for constructor

my $contract_status = $node->compile_and_deploy_contract($contract_name, $constructor_params, $src_account, $password);
my $new_contract_id = $contract_status->{contractAddress};
my $transactionHash = $contract_status->{transactionHash};
my $gas_used = hex($contract_status->{gasUsed});
print "\n", 'Contract mined.', "\n", 'Address: '.$new_contract_id, "\n", 'Transaction Hash: '.$transactionHash, "\n";

my $gas_price=$node->eth_gasPrice();
my $contract_deploy_price = $gas_used * $gas_price;
my $price_in_eth = $node->wei2ether($contract_deploy_price);
print 'Gas used: '.$gas_used.' ('.sprintf('0x%x', $gas_used).') wei, '.$price_in_eth.' ether', "\n\n";