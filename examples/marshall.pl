#!/usr/bin/perl

# web3_sha3 test

use Net::Ethereum;

my $contract_name = $ARGV[0] || 'BigBro';
my $password = 'ospasswd';
my $contract_id = '0xb3abfa488058dba76206071b3600ae6e0dec3205';

my $node = Net::Ethereum->new('http://localhost:8600/');
print "Net::Ethereum v", $node->VERSION, "\n";
$node->set_debug_mode(0);
$node->set_show_progress(1);

my $abi = $node->_read_file('build/'.$contract_name.'.abi');
$node->set_contract_abi($abi);
$node->set_contract_id($contract_id);

my @marshals = (
    $node->_marshal_int(1000),
    $node->_marshal_int(32300),
    $node->_marshal_int(0),
    $node->_marshal_int(-10),
    $node->_marshal_int(-29937),
    $node->_marshal_int(1),
    $node->_marshal_int(-19929),
    $node->_marshal_int(65535),
    $node->_marshal_int(9939),
    $node->_marshal_int(-77399773993),
    $node->_marshal_int(-65565),
    $node->_marshal_int(21365565),
    $node->_marshal_int(565),
    $node->_marshal_int(1982),
);

print join("\n", @marshals) . "\n";

my @dataset = split /\n/, $node->_read_file('./data/data.txt');
my $i;
foreach (@dataset) {
    my $_s1 = $node->_string2hex( $_ );
    my $_s2 = $node->_hex2string( $_s1 );
    # print $i++ . ":\t". $_s1 . ' = ' . $_s2 . "\n";
    print $_s1 . "\n";
}

# uint _id, string _ref, string _ip, string _ua, string _res, string _pg, string _cntry

my $f_params = {};
$f_params->{_id} = 2;
$f_params->{_ref} = 'http://perl6.pheix.org';
$f_params->{_ip} = '255.255.255.101';
$f_params->{_ua} = 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465 Safari/9537.53 BingPreview/1.0b';
$f_params->{_res} = '1900*1280';
$f_params->{_pg} = '/feedback.html';
$f_params->{_cntry} = 'US';

my $marshal = $node->_marshal( 'insert', $f_params );
print $marshal . "\n";

$f_params = {};
$f_params->{_id} = 891;
$f_params->{_ref} = 'http://yandex.ru';
$f_params->{_ip} = '70.118.120.106';
$f_params->{_ua} = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0';
$f_params->{_res} = '1200*1600';
$f_params->{_pg} = '/index.html';
$f_params->{_cntry} = 'FI';

$marshal = $node->_marshal( 'set_by_id', $f_params );
print $marshal . "\n";

